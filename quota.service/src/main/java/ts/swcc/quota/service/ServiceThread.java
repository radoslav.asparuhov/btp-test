package ts.swcc.quota.service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;

public class ServiceThread extends Thread {

    private static final String PAYLOAD_TEMPLATE = "company_id=%s&client_id=%s&grant_type=urn:ietf:params:oauth:grant-type:saml2-bearer&assertion=%s";
    
    public static HttpResponse<String> getToken() {
        ApplicationProperties props = Utils.getProperties();
        String samlAssertion = "";
        String payload = "";

        try {
            samlAssertion = SAMLAssertionGenerator.generate();
            payload = PAYLOAD_TEMPLATE.formatted(props.getCompanyId(), props.getClientId(), samlAssertion);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        HttpClient client = HttpClient.newHttpClient();

        // create a request
        HttpRequest request = HttpRequest.newBuilder(URI.create("https://apisalesdemo2.successfactors.eu/oauth/token"))
                .header("accept", "application/json").header("Content-Type", "application/x-www-form-urlencoded")
                .POST(BodyPublishers.ofString(payload)).build();

        try {
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            return response;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public void run() {
        ApplicationProperties props = Utils.getProperties();
        String samlAssertion = "";
        String payload = "";

        try {
            samlAssertion = SAMLAssertionGenerator.generate();
            payload = PAYLOAD_TEMPLATE.formatted(props.getCompanyId(), props.getClientId(), samlAssertion);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        HttpClient client = HttpClient.newHttpClient();

        // create a request
        HttpRequest request = HttpRequest.newBuilder(URI.create("https://apisalesdemo2.successfactors.eu/oauth/token"))
                .header("accept", "application/json").header("Content-Type", "application/x-www-form-urlencoded")
                .POST(BodyPublishers.ofString(payload)).build();

        try {
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            System.out.println(response.statusCode());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
