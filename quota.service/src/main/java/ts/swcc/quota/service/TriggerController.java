package ts.swcc.quota.service;

import java.net.http.HttpResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TriggerController {

    @PostMapping("/trigger")
    public ResponseEntity<String> startProcess() {
        ServiceThread st = new ServiceThread();
//        st.start();
        HttpResponse<String> response = ServiceThread.getToken();
        return new ResponseEntity<String>(response.body(), HttpStatus.OK);
    }
}
