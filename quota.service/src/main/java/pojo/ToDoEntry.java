package pojo;

public class ToDoEntry {
    private String todoEntryId;
    private String lastModifiedDateTime;
    private String dueDate;
    private String todoEntryName;
    private String categoryLabel;
    private String completedDateTime;
    private String mobileLinkUrl;
    private String userId;
    private String subjectId;
    private String createdDate;
    private String linkUrl;
    private String formDataId;
    private String categoryId;
    private String status;

    public ToDoEntry(String todoEntryId, String lastModifiedDateTime, String dueDate, String todoEntryName,
            String categoryLabel, String completedDateTime, String mobileLinkUrl, String userId, String subjectId,
            String createdDate, String linkUrl, String formDataId, String categoryId, String status) {
        super();
        this.todoEntryId = todoEntryId;
        this.lastModifiedDateTime = lastModifiedDateTime;
        this.dueDate = dueDate;
        this.todoEntryName = todoEntryName;
        this.categoryLabel = categoryLabel;
        this.completedDateTime = completedDateTime;
        this.mobileLinkUrl = mobileLinkUrl;
        this.userId = userId;
        this.subjectId = subjectId;
        this.createdDate = createdDate;
        this.linkUrl = linkUrl;
        this.formDataId = formDataId;
        this.categoryId = categoryId;
        this.status = status;
    }

    public String getTodoEntryId() {
        return todoEntryId;
    }

    public void setTodoEntryId(String todoEntryId) {
        this.todoEntryId = todoEntryId;
    }

    public String getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(String lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getTodoEntryName() {
        return todoEntryName;
    }

    public void setTodoEntryName(String todoEntryName) {
        this.todoEntryName = todoEntryName;
    }

    public String getCategoryLabel() {
        return categoryLabel;
    }

    public void setCategoryLabel(String categoryLabel) {
        this.categoryLabel = categoryLabel;
    }

    public String getCompletedDateTime() {
        return completedDateTime;
    }

    public void setCompletedDateTime(String completedDateTime) {
        this.completedDateTime = completedDateTime;
    }

    public String getMobileLinkUrl() {
        return mobileLinkUrl;
    }

    public void setMobileLinkUrl(String mobileLinkUrl) {
        this.mobileLinkUrl = mobileLinkUrl;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getFormDataId() {
        return formDataId;
    }

    public void setFormDataId(String formDataId) {
        this.formDataId = formDataId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
